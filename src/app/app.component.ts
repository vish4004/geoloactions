import { Component, OnChanges, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  private citiesList: any;
  private stateList: any;
  private cityName: any;
  private newList: any;
  private stateName = {
    City: "",
    State: "",
    District: ""
  };
  latitude: number;
  longitude: number;
  zoom: number;
  constructor(private http: HttpClient) {
    this.getCities().subscribe((data) => {
      this.citiesList = data;
      this.newList = [...new Set(this.citiesList.map(item => item.State))]
    });
  }
  title = 'cityfilter';


  ngOnInit() {
    this.setCurrentLocation();
  }


  getCities() {
    return this.http.get('https://indian-cities-api-nocbegfhqg.now.sh/cities');
  }

  getStates(e) {
    this.stateName = e;
    console.log('State Name', e);
    return this.http.get(`https://indian-cities-api-nocbegfhqg.now.sh/cities?State=${e}`).subscribe((dataNew) => {
      this.stateList = dataNew;
    });
  }

  getCity(eve) {
    this.cityName = eve;
    this.getGeoLocation();
    console.log('City name: ', this.cityName);
  }

  getGeoLocation() {
    this.http.get(`https://maps.googleapis.com/maps/api/geocode/json?address=' + 
    ${this.cityName} + '&key=AIzaSyD6LB4LPsEY0kQU59tagY5zGKhekZWPUnw`).subscribe((data => {
      console.log(data);
    }))
  };

  setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 15;
      });
    }
  }

}
